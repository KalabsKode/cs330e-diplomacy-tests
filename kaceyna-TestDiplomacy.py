#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = ["A Madrid Hold"]
        i = diplomacy_read(s)
        self.assertEqual(i,  [["A", "Madrid", "Hold"]])

    def test_read_2(self):
        s = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Support B"]
        i = diplomacy_read(s)
        self.assertEqual(i,  [["A", "Madrid", "Hold"], ["B", "Barcelona", "Move", "Madrid"], ["C", "London", "Support", "B"]])

    def test_read_3(self):
        s = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Move Madrid", "D Paris Support B", "E Austin Support A"]
        i = diplomacy_read(s)
        self.assertEqual(i,  [["A", "Madrid", "Hold"], ["B", "Barcelona", "Move", "Madrid"], ["C", "London", "Move", "Madrid"], ["D", "Paris", "Support", "B"], 
                              ["E", "Austin", "Support", "A"]])

    # ----
    # eval
    # ----
    
    def test_eval_1(self):
        v = diplomacy_eval([["A", "Madrid", "Hold"]])
        self.assertEqual(v, "A Madrid")

    def test_eval_2(self):
        v = diplomacy_eval([["A", "Madrid", "Hold"], ["B", "Barcelona", "Move", "Madrid"], ["C", "London", "Support", "B"]])
        self.assertEqual(v, ["A [dead]", "B Madrid", "C London"])

    def test_eval_3(self):
        v = diplomacy_eval([["A", "Madrid", "Hold"], ["B", "Barcelona", "Move", "Madrid"], ["C", "London", "Move", "Madrid"], ["D", "Paris", "Support", "B"],
                            ["E", "Austin", "Support", "A"]])
        self.assertEqual(v, ["A [dead]", "B [dead]", "C [dead]", "D Paris", "E Austin"])

    def test_eval_4(self):
        v = diplomacy_eval([["A", "Madrid", "Hold"], ["B", "Barcelona", "Move", "Madrid"], ["C", "London", "Support", "B"], ["D", "Austin", "Move", "London"]])
        self.assertEqual(v, ["A [dead]", "B [dead]", "C [dead]", "D [dead]"])

    def test_eval_5(self):
        v = diplomacy_eval([["A", "Madrid", "Support", "C"], ["B", "Barcelona", "Hold"], ["C", "London", "Move", "Barcelona"]])
        self.assertEqual(v, ["A Madrid", "B [dead]", "C Barcelona"])

    def test_eval_6(self):
        v = diplomacy_eval([["A", "Madrid", "Move", "Barcelona"], ["B", "Barcelona", "Hold"], ["C", "London", "Hold"]])
        self.assertEqual(v, ["A [dead]", "B [dead]", "C London"])

    def test_eval_7(self):
        v = diplomacy_eval([["A", "Madrid", "Move", "Barcelona"], ["B", "Barcelona", "Support", "C"], ["C", "London", "Hold"]])
        self.assertEqual(v, ["A [dead]", "B [dead]", "C London"])

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, "A Madrid")
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, ["A [dead]", "B Madrid", "C London"])
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, ["A [dead]", "B [dead]", "C [dead]", "D Paris", "E Austin"])
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")


    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
    


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
