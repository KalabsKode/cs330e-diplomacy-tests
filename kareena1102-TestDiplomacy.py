#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_solve(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Paris Move London\nE Austin Move Paris\nF Dallas Support B\nG Jackson Hold\nH Seattle Move Jackson\nI Miami Support H\nJ Phoenix Move Jackson\nK Portland Support J\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD [dead]\nE Paris\nF Dallas\nG [dead]\nH [dead]\nI Miami\nJ [dead]\nK Portland\n")

    def test_solve2(self):
        r = StringIO("A Barcelona Move Madrid\n B Austin Move Reno\nC Dallas Move Austin\nD Waco Move Austin\nE London Move Paris\nF Orlando Move Paris\nG Houston Support E\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Reno\nC [dead]\nD [dead]\nE Paris\nF [dead]\nG Houston\n")

    def test_solve3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\nF Waco Support A\nG Jackson Hold\nH Seattle Move Jackson\nI Miami Support H\nJ Phoenix Move Jackson\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB [dead]\nC [dead]\nD Paris\nE Austin\nF Waco\nG [dead]\nH Jackson\nI Miami\nJ [dead]\n")


if __name__ == "__main__":
    main()

''' # pragma: no cover

'''