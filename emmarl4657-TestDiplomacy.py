from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestCollatz
# -----------

class TestDiplomacy(TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold"
        a = diplomacy_read(s)
        self.assertEqual(a, ['A','Madrid','Hold'])
        #self.assertEqual(a, [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'],
                             #['C', 'London', 'Support', 'B']])

    def test_read_2(self):
        s = "B Barcelona Move Madrid"
        a = diplomacy_read(s)
        self.assertEqual(a, ['B','Barcelona','Move','Madrid'])
    
    def test_read_3(self):
        s = ""
        a = diplomacy_read(s)
        self.assertEqual(a, [])
                             
    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'],
                            ['C', 'London', 'Support', 'B']])
        self.assertEqual(v, "A [dead]\nB Madrid\nC London\n")

    def test_eval_2(self):
        v = diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'],
                            ['C', 'London', 'Support', 'B'], ['D', 'Austin', 'Move', 'London']])
        self.assertEqual(v, "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_eval_3(self):
        v = diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'],
                            ['C', 'London', 'Move', 'Madrid'], ['D', 'Paris', 'Support', 'B'],
                            ['E', 'Austin', 'Support', 'A']])
        self.assertEqual(v, "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
    def test_eval_4(self):
        v = diplomacy_eval([])
        self.assertEqual(v, "")
        
    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, 'A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n', 'A [dead]\nB Madrid\nC London\n')

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, 'A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n', 'A [dead]\nB [dead]\nC [dead]\nD [dead]\n')
        
    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, 'A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n', 
        'A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n')
                        
    # -----
    # solve
    # -----
    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")
    
    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD [dead]\n')      
    
    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n')    

# ----
# main
# ----

if __name__ == "__main__":
    main()