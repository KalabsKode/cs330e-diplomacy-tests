#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve, diplomacy_print, diplomacy_read, diplomacy_eval

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    
    # ----
    # read
    # ----

    def test_read_1(self):
        s = ['A Madrid Hold\n', 'B Barcelona Move Madrid\n', 'C London Support B\n']
        a = diplomacy_read(s)
        self.assertEqual(a,  [["A", "Madrid", "Hold"], ["B", "Barcelona", "Move", "Madrid"], ["C", "London", "Support", "B"]])

    def test_read_2(self):
        s = ['A Madrid Hold\n', 'B Barcelona Move Madrid\n']
        a = diplomacy_read(s)
        self.assertEqual(a,  [["A", "Madrid", "Hold"], ["B", "Barcelona", "Move", "Madrid"]])

    def test_read_3(self):
        s = ['A Madrid Hold\n', 'B Barcelona Move Madrid\n', 'C London Move Madrid\n', 'D Paris Support B\n']
        a = diplomacy_read(s)
        self.assertEqual(a,  [["A", "Madrid", "Hold"], ["B", "Barcelona", "Move", "Madrid"], ["C", "London", "Move", "Madrid"], ["D", "Paris", "Support", "B"]])

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, ["A [dead]", "B Madrid", "C London"])
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, ["A [dead]", "C London", "B Madrid"])
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, ["A [dead]", "B [dead]"])
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_print_4(self):
        w = StringIO()
        diplomacy_print(w, ["A [dead]", "B Madrid", "C [dead]", "D Paris"])
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")
    
    # -----
    # eval
    # -----

    def test_eval_1(self):
        input_list = [["A", "Madrid", "Hold"], ["B", "Barcelona", "Move", "Madrid"], ["C", "London", "Support", "B"]]
        a = diplomacy_eval(input_list)
        self.assertEqual(a, ["A [dead]", "B Madrid", "C London"])

    def test_eval_2(self):
        input_list = [["A", "Madrid", "Hold"], ["B", "Barcelona", "Move", "Madrid"]]
        a = diplomacy_eval(input_list)
        self.assertEqual(a, ["A [dead]", "B [dead]"])

    def test_eval_3(self):
        input_list = [["A", "Madrid", "Hold"], ["B", "Barcelona", "Move", "Madrid"], ["C", "London", "Move", "Madrid"], ["D", "Paris", "Support", "B"]]
        a = diplomacy_eval(input_list)
        self.assertEqual(a, ["A [dead]", "B Madrid", "C [dead]", "D Paris"])

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_solve_5(self):
        r = StringIO("A Boston Support C\nB Austin Hold\nC Chicago Move Austin")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Boston\nB [dead]\nC Austin\n")

    def test_solve_6(self):
        r = StringIO("A Madrid Move Pluto\nB Paris Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Pluto\nB Paris\n")

    def test_solve_7(self):
        r = StringIO("A Madrid Move Paris\nB Paris Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Paris\nB Madrid\n")

    def test_solve_8(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_9(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support A\nD Austin Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC London\nD Austin\n")

    def test_solve_10(self):
        r = StringIO("A Madrid Hold\nB Barcelona Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Barcelona\n")

    def test_solve_11(self):
        r = StringIO("A Madrid Support B\nB Barcelona Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Barcelona\n")

    def test_solve_12(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move London\nC London Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB [dead]\nC [dead]\n")

    def test_solve_13(self):
        r = StringIO("A Madrid Hold\nC London Support B\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

# ----
# main
# ----

if __name__ == "__main__": #pragma: no cover
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......................
----------------------------------------------------------------------
Ran 23 tests in 0.002s

OK

$ coverage report -m                   >> TestDiplomacy.out


$ cat TestDiplomacy.out
.......................
----------------------------------------------------------------------
Ran 23 tests in 0.002s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          76      0     40      0   100%
TestDiplomacy.py     109      0      0      0   100%
--------------------------------------------------------------
TOTAL                185      0     40      0   100%
"""

