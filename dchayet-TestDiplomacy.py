#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C)
# Andy Kim and Daniel Chayet
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy(TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        w = diplomacy_read("A Madrid Hold")
        self.assertEqual(w, ["A", "Madrid", "Hold", None, 0])

    def test_read_2(self):
        w = diplomacy_read("B Barcelona Move Madrid")
        self.assertEqual(w, ["B", "Barcelona", "Move", "Madrid", 0])
    
    def test_read_3(self):
        w = diplomacy_read("C London Support B")
        self.assertEqual(w, ["C", "London", "Support", "B", 0])
    
    def test_read_4(self):
        w = diplomacy_read("D Austin Move London")
        self.assertEqual(w, ["D", "Austin", "Move", "London", 0])

    def test_read_5(self):
        w = diplomacy_read("C London Move Madrid")
        self.assertEqual(w, ["C", "London", "Move", "Madrid", 0])
    
    def test_read_6(self):
        w = diplomacy_read("D Paris Support B")
        self.assertEqual(w, ["D", "Paris", "Support", "B", 0])
    
    # -----
    # eval
    # -----

    # corner case where two armies swap
    def test_eval_1(self):
        scenario = {'A': ['Madrid', 'Move', 'Barcelona', 0], 'B': ['Barcelona', 'Move', 'Madrid', 0]}
        results = diplomacy_eval(scenario)
        self.assertEqual({'A': 'Barcelona', 'B': 'Madrid'}, results)
    
    # armies move in a circle
    def test_eval_2(self):
        scenario = {'A': ['Madrid', 'Move', 'Barcelona', 0], 'B': ['Barcelona', 'Move', 'Girona', 0], 'C': ['Girona', 'Move', 'Madrid', 0]}
        results = diplomacy_eval(scenario)
        self.assertEqual({'A': 'Barcelona', 'B': 'Girona', 'C': 'Madrid'}, results)
    
    # something relatively normal
    def test_eval_3(self):
        scenario = {'A': ['Madrid', 'Hold', None, 0], 'B': ['Girona', 'Support', 'A', 0], 'C': ['Barcelona', 'Support', 'A', 0], 'D': ['Tarragona', 'Support', 'A', 0], 'E': ['Seville', 'Support', 'A', 0], 'F': ['Cordoba', 'Move', 'Madrid', 0]}
        results = diplomacy_eval(scenario)
        self.assertEqual({'A': 'Madrid', 'B': 'Girona', 'C': 'Barcelona', 'D': 'Tarragona', 'E': 'Seville', 'F': '[dead]'}, results)
    

    # -----
    # print
    # -----

    # all dead
    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, {'A': '[dead]', 'B': '[dead]',\
                            'C': '[dead]', 'D': '[dead]'})
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD [dead]\n')

    # only one army and it is dead
    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, {'A': '[dead]'})
        self.assertEqual(w.getvalue(), 'A [dead]\n')

    # only one army and it lives 
    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, {'B': 'SanFrancisco'})
        self.assertEqual(w.getvalue(), 'B SanFrancisco\n')

    # only some armies alive
    def test_print_4(self):
        w = StringIO()
        diplomacy_print(w, {'A': '[dead]', 'B': 'MexicoCity',\
                            'C': '[dead]', 'D': 'Acapulco'})
        self.assertEqual(w.getvalue(), 'A [dead]\nB MexicoCity\nC [dead]\nD Acapulco\n')

    # four armies, all alive
    def test_print_5(self):
        w = StringIO()
        diplomacy_print(w, {'A': 'Johannesburg', 'B': 'Lagos',\
                            'C': 'Ougadougou', 'D': 'Cairo'})
        self.assertEqual(w.getvalue(), 'A Johannesburg\nB Lagos\nC Ougadougou\nD Cairo\n')

    # one alive, one dead, names don't go in order
    def test_print_6(self):
        w = StringIO()
        diplomacy_print(w, {'B': '[dead]', 'D': 'Boston'})
        self.assertEqual(w.getvalue(), 'B [dead]\nD Boston\n')


    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_solve_5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")
        
    # corner case, swap
    def test_solve_6(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Barcelona\nB Madrid\n")
    
    # corner case, 3 in circle
    def test_solve_7(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Move Girona\nC Girona Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Barcelona\nB Girona\nC Madrid\n")
    
    # corner case, large support
    def test_solve_8(self):
        r = StringIO("A Madrid Hold\nB Girona Support A\nC Barcelona Support A\nD Tarragona Support A\nE Seville Support A\nF Cordoba Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Girona\nC Barcelona\nD Tarragona\nE Seville\nF [dead]\n")
    
    # corner case, target is city not in original list
    def test_solve_9(self):
        r = StringIO("A Barcelona Hold\nB Madrid Support A\nC Teruel Move Zaragoza")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Barcelona\nB Madrid\nC Zaragoza\n")

# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
