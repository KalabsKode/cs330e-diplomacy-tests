from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_eval, diplomacy_solve, diplomacy_read, diplomacy_print

class TestDiplomacy(TestCase):

    def test_eval_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Support A\nC Houston Support B\nD Austin Support A\nE Tokyo Move Madrid\nF Dallas Support E")
        w = StringIO("")
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\nC Houston\nD Austin\nE [dead]\nF Dallas\n")

    def test_eval_2(self):
        r = StringIO("A Tokyo Move Madrid \nB Barcelona Support A\nC Houston Support E\nD Austin Support A\nE Madrid Hold\nF Dallas Support E")
        w = StringIO("")
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(),"A [dead]\nB Barcelona\nC Houston\nD Austin\nE [dead]\nF Dallas\n")

    def test_eval_3(self):
        r = StringIO("A Tokyo Hold\nB Beijing Support A\nC London Support B\nD Austin Move Beijing\nE NewYork Move Tokyo")
        w = StringIO("")
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(),"A [dead]\nB Beijing\nC London\nD [dead]\nE [dead]\n")

if __name__ == "__main__": #pragma: no cover
    main()