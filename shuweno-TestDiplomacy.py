#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.6/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestCollatz
# -----------


class TestDiplomacy (TestCase):
    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_solve_3(self):
        r = StringIO("A Paris Move London\nB Frankfurt Hold\nC London Support B")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Frankfurt\nC [dead]\n")

    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_solve_5(self):
        r = StringIO("A Tokyo Hold\nB Kyoto Move Tokyo\nC Osaka Move Tokyo")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_solve_6(self):
        r = StringIO("A Berlin Move Barcelona\nB Barcelona Move Berlin")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Barcelona\nB Berlin\n")

    def test_solve_7(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_8(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

# ----
# main
# ----


if __name__ == "__main__": #pragma: no cover
    main()

""" pragma: no cover

$ make TestDiplomacy.out
coverage run    --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1
coverage report -m                      >> TestDiplomacy.out
cat TestDiplomacy.out
........
----------------------------------------------------------------------
Ran 8 tests in 0.002s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          57      0     38      2    98%   65->67, 76->exit
TestDiplomacy.py      44      0      0      0   100%
--------------------------------------------------------------
TOTAL                101      0     38      2    99%
"""