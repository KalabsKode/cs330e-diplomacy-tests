# -*- coding: utf-8 -*-
"""
Created on Thu Oct 20 17:30:33 2022

@author: agatp
"""

# ----age
# main
# ----
from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_solve
from Diplomacy import calc_dict
from Diplomacy import create_dict
from Diplomacy import diplomacy_read
from Diplomacy import diplomacy_print

class TestDiplomacy (TestCase):

    ## diplomacy_read

    def test_diplomacy_read_1(self):
        s = "A Minneapolis Hold B"
        test = diplomacy_read(s)
        self.assertEqual(test, ["A Minneapolis Hold B"])

    def test_diplomacy_read_2(self):
        s = "D Austin Support B"
        test = diplomacy_read(s)
        self.assertEqual(test, ["D Austin Support B"])

    def test_diplomacy_read_3(self):
        s = "Z Dallas Move A"
        test = diplomacy_read(s)
        self.assertEqual(test, ["Z Dallas Move A"])
    
    ## create_dict

    def test_createDict_1(self):
        a = "A Madrid Hold"
        b = "B Barcelona Move Madrid"
        c = "C London Move Madrid"
        d = "D Paris Support B"
        e = "E Austin Support A"
        test = create_dict([a,b,c,d,e])
        self.assertEqual(test, {'A': ['Madrid', [False, 'Madrid'], ['E']],
                                'B': ['Barcelona', [True, 'Madrid'], ['D']],
                                'C': ['London', [True, 'Madrid'], []],
                                'D': ['Paris', [False, 'Paris'], []],
                                'E': ['Austin', [False, 'Austin'], []]})
        
    def test_createDict_2(self):
        a = "A Madrid Hold"
        b = "B Barcelona Hold"
        c = "C London Move Madrid"
        test = create_dict([a,b,c])
        self.assertEqual(test, {'A': ['Madrid', [False, 'Madrid'], []],
                                'B': ['Barcelona', [False, 'Barcelona'], []],
                                'C': ['London', [True, 'Madrid'], []]})
        
    def test_createDict_3(self):
        a = "A Dallas Hold"
        b = "B Austin Hold"
        c = "C Houston Move Austin"
        d = "D Toronto Move Austin"
        e = "E Tampa Move Dallas"
        f = "F Venice Support B"
        g = "G Munich Support B"
        h = "H Rome Support A"
        i = "I Orlando Support A"
        
        test = create_dict([a,b,c,d,e,f,g,h,i])
        self.assertEqual(test, {'A': ['Dallas', [False, 'Dallas'], ['H','I']],
                                'B': ['Austin', [False, 'Austin'], ['F','G']],
                                'C': ['Houston', [True, 'Austin'], []],
                                'D': ['Toronto', [True, 'Austin'], []],
                                'E': ['Tampa', [True, 'Dallas'], []],
                                'F': ['Venice', [False, 'Venice'], []],
                                'G': ['Munich', [False, 'Munich'], []],
                                'H': ['Rome', [False, 'Rome'], []],
                                'I': ['Orlando', [False, 'Orlando'], []]})
    
    ## calc_dict
    def test_calcDict_1(self):
        a = {'A': ['Madrid', [False, 'Madrid'], ['E']],
             'B': ['Barcelona', [True, 'Madrid'], ['D']],
             'C': ['London', [True, 'Madrid'], []],
             'D': ['Paris', [False, 'Paris'], []],
             'E': ['Austin', [False, 'Austin'], []]}
        test = calc_dict(a)
        self.assertEqual(test, {'A': ['[dead]', [False, 'Madrid'], ['E']],
                                'B': ['[dead]', [True, 'Madrid'], ['D']],
                                'C': ['[dead]', [True, 'Madrid'], []],
                                'D': ['Paris', [False, 'Paris'], []],
                                'E': ['Austin', [False, 'Austin'], []]})

    def test_calcDict_2(self):
        a = {'A': ['Munich', [False, 'Munich'], []],
             'B': ['Rome', [True, 'Munich'], []],
             'C': ['Venice', [True, 'Munich'], []]}
        test = calc_dict(a)
        self.assertEqual(test, {'A': ['[dead]', [False, 'Munich'], []],
                                'B': ['[dead]', [True, 'Munich'], []],
                                'C': ['[dead]', [True, 'Munich'], []]})

    def test_calcDict_3(self):
        a = {'A': ['Dallas', [False, 'Dallas'], ['H','I']],
             'B': ['Austin', [False, 'Austin'], ['F','G']],
             'C': ['Houston', [True, 'Austin'], []],
             'D': ['Toronto', [True, 'Austin'], []],
             'E': ['Tampa', [True, 'Dallas'], []],
             'F': ['Venice', [False, 'Venice'], []],
             'G': ['Munich', [False, 'Munich'], []],
             'H': ['Rome', [False, 'Rome'], []],
             'I': ['Orlando', [False, 'Orlando'], []]}
        test = calc_dict(a)
        self.assertEqual(test, {'A': ['Dallas', [False, 'Dallas'], ['H','I']],
                                'B': ['Austin', [False, 'Austin'], ['F','G']],
                                'C': ['[dead]', [True, 'Austin'], []],
                                'D': ['[dead]', [True, 'Austin'], []],
                                'E': ['[dead]', [True, 'Dallas'], []],
                                'F': ['Venice', [False, 'Venice'], []],
                                'G': ['Munich', [False, 'Munich'], []],
                                'H': ['Rome', [False, 'Rome'], []],
                                'I': ['Orlando', [False, 'Orlando'], []]})

    ## diplomacy_print
    def test_diplomacy_print_1(self):
        w = StringIO()
        r = ["A Madrid Hold"]
        diplomacy_print(w,r)
        self.assertEqual(w.getvalue(), "A Madrid Hold\n")

    def test_diplomacy_print_2(self):
        w = StringIO()
        r = ["A [dead]","B Madrid", "C London"]
        diplomacy_print(w,r)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_diplomacy_print_3(self):
        w = StringIO()
        r = ["A [dead]", "B [dead]", "C [dead]", "D [dead]"]
        diplomacy_print(w,r)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
        
        

    ## diplomacy_solve
    
    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")
        
    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
    
    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")
        
    def test_solve_5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
    
    def test_solve_6(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")
        
    def test_solve_7(self):
        r = StringIO("A Barcelona Move Madrid\nB Madrid Hold\nC Lisbon Move Barcelona\nD Vienna Move Barcelona\nE Berlin Support D")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Barcelona\nE Berlin\n")
        
    def test_solve_8(self):
        r = StringIO("A Barcelona Move Madrid\nB Madrid Move Barcelona\nC Lisbon Move Barcelona\nD Vienna Move Barcelona\nE Berlin Support D")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Madrid\nB [dead]\nC [dead]\nD Barcelona\nE Berlin\n")
        
    def test_solve_9(self):
        r = StringIO("A Tokyo Hold\nB Beijing Support A\nC London Support B\nD Austin Move Beijing\nE NewYork Move Tokyo")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Beijing\nC London\nD [dead]\nE [dead]\n")
        
    def test_solve_10(self):
        r = StringIO("A Tokyo Hold\nB Beijing Support A\nC London Support B\nD Austin Move Tokyo\nE NewYork Support D")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Beijing\nC London\nD [dead]\nE NewYork\n")   
        
    def test_solve_11(self):
        r = StringIO("A Barcelona Support B\nB Madrid Move Berlin")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Barcelona\nB Berlin\n")
    
if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
